<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{
    public function create() {
        return view('profile.create');
    }

    public function index($id) {
        $profile = DB::table('profile')->where('user_id',$id)->get();
        $users = DB::table('users')->where('id',$id)->first(); //select * from
        // dd($profile);
        return view('profile.index', compact('profile', 'users'));
    }

    public function store(Request $request) {

        $request->validate([
            'alamat' => 'required',
            'nohp' => 'required',      
        ]);
        $query = DB::table('profile')->insert(
            ["alamat" => $request["alamat"],
            "nohp" => $request["nohp"],
            "user_id" => $request["userid"]
            ]
        );
        return redirect ('/home')->with('success', 'Berhasil Menambah Produk!');
         
    }
    public function edit($id) {
        $profile = DB::table('profile')->where('id', $id)->first();
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'alamat' => 'required',
            'nohp' => 'required', 
        ]);
        $query = DB::table('profile')
        ->where('id', $id)
        ->update([
            "alamat" => $request["alamat"],
            "nohp" => $request["nohp"]
        ]);
        
        return redirect ("/profile/".$id)->with('success', 'Berhasil Mengupdate Produk No '. $id);
    }
}
