
@extends('adminlte.master')
@section('content')
<div class ="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Produk Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role ="form" action="/produk" method ="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="namaproduk">Nama Produk</label>
                    <input type="text" class="form-control" id="namaproduk" name="namaproduk" placeholder="Masukkan Nama Produk">
                    @error('namaproduk')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" placeholder="Masukkan Harga">
                    @error('harga')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>                      
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat</button>
                </div>
              </form>
            </div>
</div>
@endsection

