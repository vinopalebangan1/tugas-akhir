@extends('adminlte.master')

@section('content')
        <div class ="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Profil {{ Auth::user()->name }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role ="form" action="/profile/{{$profile->id}}" method ="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="{{old('alamat', $profile->alamat)}}" placeholder="Masukkan Alamat">
                    @error('alamat')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="nohp">Nomor HP</label>
                    <input type="text" class="form-control" id="nohp" name="nohp" value="{{old('nohp', $profile->nohp)}}"placeholder="Masukkan Nomor HP">
                    @error('nohp')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>                           
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
        </div>
@endsection