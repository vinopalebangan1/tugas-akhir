@extends('adminlte.master')

@section('content')
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Profile Anda</h3>
              </div>
              <div class= "ml-3 mt-3">
                  <h4>Nama = {{$users->name}}</h4>
                  @forelse($profile as $key => $keyprofile)
                    <p>Alamat = {{$keyprofile->alamat}}</p>
                    <p>No Handphone = {{$keyprofile->nohp}}</p>
                    @empty
                    <p>Tidak Ada Profil</p>
                    <a href="/profile/create" class="btn btn-warning btn-sm mr-2 mb-2">Tambah Data Profil</a>
                  @endforelse
              </div>
            </div>
@endsection