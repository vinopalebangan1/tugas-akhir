
@extends('adminlte.master')
@section('content')
<div class ="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Produk Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role ="form" action="/profile/{{Auth::user()->id}}" method ="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat">
                    @error('alamat')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="nohp">No HP</label>
                    <input type="text" class="form-control" id="nohp" name="nohp" placeholder="Masukkan No HP">
                    @error('nohp')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>  
                  <div class="form-group">
                    <label for="userid">User ID</label>
                    <input type="text" class="form-control" id="userid" name="userid" value="{{ Auth::user()->id }}"placeholder="Masukkan User ID">
                    @error('userid')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>                     
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat</button>
                </div>
              </form>
            </div>
</div>
@endsection

